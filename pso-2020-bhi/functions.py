import numpy as np


def sphere(x):

    j = (x ** 2.0).sum(axis=1)

    return j