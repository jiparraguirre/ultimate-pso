# pso-2020-bhi
## Getting started

Prerequisites:

* Python 3
* [See TensorFlow install guide](https://www.tensorflow.org/install)
* [See JAX install guide](https://github.com/google/jax#installation)


```
#!
virtualenv --system-site-packages -p python3 ./venv
source venv/bin/activate
pip install pyswarms
pip install --upgrade jax jaxlib
pip install --upgrade tensorflow
sh run.sh 
```
